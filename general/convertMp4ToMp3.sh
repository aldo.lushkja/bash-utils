#!/bin/bash

set -euo pipefail

gum style \
	--foreground 212 --border-foreground 212 --border double \
	--align center --width 50 --margin "1 2" --padding "2 4" \
	'Mp4 -> Mp3' 'Convert from mp4 to mp3' 'v.0.0.1'
echo "Select the file you want to convert :file_folder:" | gum format -t emoji 
INPUT=$(gum file $PWD)
echo "File selected" $INPUT
if [[ $INPUT == *"mp4"* ]];then
	gum confirm "Do you want to proceed with the conversion?"
	if [ $? == 0 ];then
		gum spin --spinner line --title "Conversion is progress..." -- ffmpeg -i ${INPUT} -vn -acodec libmp3lame -ac 2 -ab 160k -ar 48000 generated/out_$(date +%s).mp3
		echo "file converted successfully :heavy_check_mark:" | gum format -t emoji
	else
        	echo "Operation aborted."
        	exit 1
	fi
else
	echo "Selection is not a valid mp4 file, please retry. :speak_no_evil:" | gum format -t emoji
fi


