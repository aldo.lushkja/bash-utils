# Bash utils

A collection of some useful bash scripts

### Table of contents

1. convertMp4ToMp3.sh - converts an input mp4 file to mp3 file


#### TODO

- [ ] Other conversions audio,video,text
- [ ] Useful file system handling utility
- [ ] Git utility scripts
- [ ] AWS utility scripts
